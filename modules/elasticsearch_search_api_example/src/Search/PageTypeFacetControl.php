<?php

namespace Drupal\elasticsearch_search_api_example\Search;

use Drupal\elasticsearch_search_api\Search\FacetedSearchActionInterface;
use Drupal\elasticsearch_search_api\Search\SearchResult;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\elasticsearch_search_api\Search\Facet\Control\TermFacetBase;
use Drupal\elasticsearch_search_api\Search\Facet\FacetValueMetaDataTreeStorageInterface;

/**
 * Class PageTypeFacetControl.
 */
class PageTypeFacetControl extends TermFacetBase {

  use StringTranslationTrait;

  const VOCABULARY_ID = 'page_type';
  const PROPERTY_PATH = 'page_type';

  /**
   * ThemeFacetControl constructor.
   */
  public function __construct(FacetValueMetaDataTreeStorageInterface $facetValueMetaDataTreeStorage, string $routeName, EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($facetValueMetaDataTreeStorage, $routeName, $entityTypeManager);

    $this->setVocabulary(self::VOCABULARY_ID);
    $this->setfacetValuesSortMethod(self::SORT_TERM_WEIGHT);
    $this->setCanSelectMultiple(TRUE);
    $this->setEnableHierarchy(TRUE);
    $this->setIncludeEmptyFacets(FALSE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldName(): string {
    return self::PROPERTY_PATH;
  }

  /**
   * {@inheritdoc}
   */
  public function addToAggregations(): bool {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFacetTitle() {
    return sprintf('<h2>%s</h2>', $this->t('Page type'));
  }

  /**
   * {@inheritdoc}
   */
  public function build(string $facet, FacetedSearchActionInterface $searchAction, SearchResult $result): array {
    $terms = $this->facetValueMetaDataTreeStorage->loadTopLevel();
    $facetCounts = $result->getFacetCounts($facet);

    return $this->buildFacetsFromTerms($facet, $terms, $facetCounts, $searchAction, $this->getFacetTitle(), FALSE, TRUE);
  }

  /**
   * {@inheritdoc}
   */
  protected function buildChildren(int $parentId, string $facet, array $facetCounts, FacetedSearchActionInterface $searchAction) {
    $terms = $this->facetValueMetaDataTreeStorage->loadChildren($parentId);

    if (empty($terms)) {
      return NULL;
    }

    return $this->buildFacetsFromTerms($facet, $terms, $facetCounts, $searchAction, '', TRUE, TRUE);
  }

}
