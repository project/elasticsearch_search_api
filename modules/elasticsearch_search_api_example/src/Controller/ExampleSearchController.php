<?php

namespace Drupal\elasticsearch_search_api_example\Controller;

use Drupal\elasticsearch_search_api\Controller\SearchController;
use Drupal\elasticsearch_search_api\Form\SearchForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ExampleSearchController.
 */
class ExampleSearchController extends SearchController {

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('renderer'),
      $container->get('current_route_match'),
      $container->get('elasticsearch_search_api_example.search_action_factory'),
      $container->get('elasticsearch_search_api_example.elasticsearch_params_builder'),
      $container->get('elasticsearch_search_api_example.elasticsearch_result_parser'),
      $container->get('elasticsearch_search_api.suggest.title_suggester'),
      $container->get('elasticsearch_search_api_example.search_repository'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getSearchFormClassName() {
    return SearchForm::class;
  }

  /**
   * {@inheritdoc}
   */
  protected function getFacets() {
    return [
      'page_type',
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getSearchHeader() {
    return [
      '#type' => 'container',
      '#children' => $this->formBuilder()->getForm($this->getSearchFormClassName(), FALSE, 'web-search', 'web-ajax-search-form', TRUE, $this->searchRouteName),
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function getSearchRouteName() {
    return 'elasticsearch_search_api_example.search';
  }

  /**
   * {@inheritdoc}
   */
  protected function getFilterRouteName() {
    return 'elasticsearch_search_api_example.filter';
  }

}
