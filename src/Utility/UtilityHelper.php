<?php

namespace Drupal\elasticsearch_search_api\Utility;

use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Provides specific helper methods.
 */
class UtilityHelper {

  /**
   * Helper function to extract a quoted string from a search term.
   *
   * @param $keyword
   *   A search term.
   *
   * @return array
   */
  public static function extractQuotedString(&$keyword) {
    preg_match_all('/\"([^\"]*?)\"/', $keyword, $matches);
    foreach ($matches[0] as $phrase) {
      $keyword = str_replace($phrase, '', $keyword);
      $keyword = trim($keyword);
    }
    return $matches[1];
  }

}
